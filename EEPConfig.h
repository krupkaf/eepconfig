#ifndef EEPCONFIG_H
#define EEPCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void EEPConfigInit(void *config, void *defaultConfig, uint16_t sizeOfConfig);
void EEPConfigReadNoCRC(void);
void EEPConfigReadDefault(void);
void EEPConfigRead(void);
void EEPConfigSave(void);

#ifdef __cplusplus
}
#endif

#endif
