#include "EEPConfig.h"
#include "crc.h"
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

void *EEPCconfig;
void *EEPCdefaultConfig = 0;
uint16_t EEPCsizeOfConfig;

uint16_t EEPConfigCRC(void) {
  uint16_t crc = 0xFFFF;
  uint16_t i;
  const uint8_t *p = 0;

  for (i = 0; i < EEPCsizeOfConfig; i++) {
    uint16_t crc16(uint16_t crc, uint8_t data);
    crc = crc16(crc, eeprom_read_byte(p++));
  }
  return crc;
}

uint8_t EEPConfigCRCTest(void) {
  if (eeprom_read_word((const uint16_t *)EEPCsizeOfConfig) == EEPConfigCRC()) {
    return 1;
  }
  return 0;
}

void EEPConfigInit(void *config, void *defaultConfig, uint16_t sizeOfConfig) {
  EEPCconfig = config;
  EEPCsizeOfConfig = sizeOfConfig;
  EEPCdefaultConfig = defaultConfig;
  EEPConfigRead();
}

void EEPConfigReadNoCRC(void) {
  eeprom_read_block((void *)EEPCconfig, (void *)0, EEPCsizeOfConfig);
};

void EEPConfigRead(void) {
  if (EEPConfigCRCTest() != 0) {
    EEPConfigReadNoCRC();
  } else {
    EEPConfigReadDefault();
  }
};

void EEPConfigSave(void) {
  eeprom_update_block((void *)EEPCconfig, (void *)0, EEPCsizeOfConfig);
  eeprom_update_word((uint16_t *)EEPCsizeOfConfig, EEPConfigCRC());
};

void EEPConfigReadDefault(void) {
  if (EEPCdefaultConfig != 0) {
    uint8_t *buf = (uint8_t *)EEPCdefaultConfig;
    uint8_t *c = (uint8_t *)EEPCconfig;
    uint16_t i;
    for (i = 0; i < EEPCsizeOfConfig; i++) {
      *c = (uint8_t)pgm_read_byte(buf);
      buf++;
      c++;
    };
  }
}
